# MsDev Gitlab - Dependency scanning for JS/TS projects
This is a generic pipeline step for scanning dependencies on a NPM project, it can be extended for other package managers.
The pipeline step runs the package manager audit command, and stores the `npm-audit.txt` as artifact.

Sadly no merge request widgets are possible due to how Gitlab locked down their featureset to the crazy expensive `Ultimate`.

# Usage
To use the pipeline step, add the following to your `.gitlab-ci.yml`:
```yaml
include:
  - remote: https://gitlab.com/guido19/msdev-js-package-dependency-analyzer-pipeline/-/blob/main/dependency-scanning.gitlab-ci.yml
```
Optionally override the `node` image version, `node:16` is the default right now:
```
variables:
  NODE_IMAGE: node:16
```